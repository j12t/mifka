CREATE TABLE queue (
    topic   VARCHAR(64) NOT NULL,
    id      VARCHAR(32) NOT NULL,
    type    VARCHAR(64),
    content MEDIUMBLOB,

    PRIMARY KEY( id )
);

CREATE TABLE topic (
    id   INT         NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL UNIQUE,

    PRIMARY KEY( id )
);
