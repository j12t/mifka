#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import re
from enum import Enum


class BeforeAfterHere(Enum):
    """
    Express that we want the element at this cursor, or the next one
    after or the one before.
    """
    HERE = 0
    BEFORE = -1
    AFTER = 1



class MifkaCursor:
    """
    An opaque abstraction for a location in the Queue.
    """
    def __init__( self, beforeAfterHere = BeforeAfterHere.HERE ) :
        self.beforeAfterHere = beforeAfterHere


    def atCurrentStart() :
        return OffsetFromStartMifkaCursor( 0, BeforeAfterHere.HERE )


    def atCurrentEnd() :
        return OffsetFromEndMifkaCursor( 0, BeforeAfterHere.HERE )


    def getBeforeAfterHere(self) :
        return self.beforeAfterHere


    def parseCursor( s ) :
        """
        Create a MifkaCursor based on a string specification

        s: the string
        return: the cursor
        """
        s = s.strip()

        if s[-1] == '+':
            beforeAfterHere = BeforeAfterHere.AFTER
            s = s[:-1]

        elif s[-1] == '-':
            beforeAfterHere = BeforeAfterHere.BEFORE
            s = s[:-1]

        else:
            beforeAfterHere = BeforeAfterHere.HERE

        m = re.match( '\s*^(-)?(\d+)\s*$', s )
        if m is None:
            ret = KeyMifkaCursor( s, beforeAfterHere )

        else:
            if m.group( 1 ) == '-' :
                ret = OffsetFromEndMifkaCursor( int( m.group(2) ), beforeAfterHere )

            else :
                ret = OffsetFromStartMifkaCursor( int( m.group(2) ), beforeAfterHere )

        return ret


class KeyMifkaCursor( MifkaCursor ) :
    """
    This implementation is based on a high-res time stamp with leading 't'.
    If the id is None, it means "at the very beginning of the queue".
    """

    def __init__(self, id, beforeAfterHere = BeforeAfterHere.HERE ) :
        """
        Initialize.

        id: the id
        """
        MifkaCursor.__init__( self, beforeAfterHere )

        self.id = id


    def fromTs( ts ) :
        """
        Factory method
        """
        return KeyMifkaCursor( 't' + str( ts ), BeforeAfterHere.HERE )


    def getId(self) :
        """
        Convert back to string

        return: string
        """
        return self.id


    def __str__(self) :
        return 'KeyMifkaCursor( %s, %s )' % ( self.id, self.beforeAfterHere )



class OffsetMifkaCursor( MifkaCursor ) :
    def __init__( self, beforeAfterHere ) :
        MifkaCursor.__init__( self, beforeAfterHere )



class OffsetFromStartMifkaCursor( OffsetMifkaCursor ) :
    """
    This implementation is an offset relative to the beginning of the queue.
    """
    def __init__( self, offset, beforeAfterHere = BeforeAfterHere.HERE ) :
        """
        Initialize

        offset: the offset
        """
        if offset < 0 :
            raise ValueError( 'Offset must not be negative' )

        OffsetMifkaCursor.__init__( self, beforeAfterHere )

        self.offset = offset


    def getOffset( self ) :
        return self.offset


    def __str__(self) :
        return 'OffsetFromStartMifkaCursor( %d )' % self.offset



class OffsetFromEndMifkaCursor( OffsetMifkaCursor ) :
    """
    This implementation is an offset relative to the end of the queue.
    """
    def __init__( self, offset, beforeAfterHere = BeforeAfterHere.HERE ) :
        """
        Initialize

        offset: the offset
        """
        if offset < 0 :
            raise ValueError( 'Offset must not be negative' )

        OffsetMifkaCursor.__init__( self, beforeAfterHere )

        self.offset = offset


    def getOffset( self ) :
        return self.offset


    def __str__(self) :
        return 'OffsetFromEndMifkaCursor( %d )' % self.offset
