#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import re
import ubos.logging
import ubos.utils

from mifka.MifkaCursor import *

class MifkaTopic :
    """
    Represents a topic
    """

    def __init__( self, id, name, service ) :
        """
        Init.

        id: id of the topic
        name: name of the topic
        service: the MifkaService this is a topic of
        """
        self.id      = id
        self.name    = name
        self.service = service


    def getName( self ) :
        """
        Obtain the name of this topic

        return: name
        """
        return self.name


    def publish( self, contentType, content ) :
        """
        Publish content of type type.

        contentType: the type of content
        content: the content
        return: True if successful
        """
        return self.service.publish( self, contentType, content )


    def republish( self, message ) :
        """
        Republish a message to this topic. This will cause the identifier
        to change, but everything else remaining the same

        msg: the message to republish
        return: True if successful
        """
        return self.service.republish( self, message )


    def getRange( self, r ) :
        """
        Obtain the elements in this range

        r: the range
        return: the elements
        """
        return self.service.getRange( self, r )


    def deleteRange( self, r ) :
        """
        Delete the elements in this range

        r: the range
        return: number of deleted messages
        """
        return self.service.deleteRange( self, r )
