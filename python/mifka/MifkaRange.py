#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import re
import ubos.logging
import ubos.utils

from mifka.MifkaCursor import BeforeAfterHere, MifkaCursor

class MifkaRange :
    """
    A MifkaRange is a range between two locations in the queue each of
    which is specified with a MifkaCursor. The range is inclusive of
    both end points.
    """
    def __init__( self, start, end ) :

        if start.getBeforeAfterHere() == BeforeAfterHere.BEFORE :
            ubos.logging.fatal( 'Cannot start range with a - cursor' )

        if end.getBeforeAfterHere() == BeforeAfterHere.AFTER :
            ubos.logging.fatal( 'Cannot end range with a + cursor' )

        self.start = start
        self.end   = end


    def getStart( self ) :
        """
        Obtain the start cursor

        return: start cursor
        """
        return self.start


    def getEnd( self ) :
        """
        Obtain the end cursor

        return: end cursor
        """
        return self.end


    def __str__( self ) :
        return "{ " + str( self.start ) + " .. " + str( self.end ) + " }"


    def fullRange() :
        """
        Obtain a default range that includes all possible elements

        return: the range
        """
        return MifkaRange( MifkaCursor.atCurrentStart(), MifkaCursor.atCurrentEnd() )


    def parseRange( s ) :
        """
        Parse a range specification in string format.

        s: the string containing the range specification
        return: the range
        """
        ubos.logging.trace( 'parseRange: "%s"' % str(s) )

        if isinstance( s, list ) :
            s = ' '.join( s )

        if s:
            s = s.strip()

        if s is None or s == '':
            return MifkaRange.fullRange()

        m = re.match( '^([^\s]*)\s*\.\.\s*([^\s]*)$', s )
        if m:
            startS = m.group(1)
            endS   = m.group(2)

            if startS == '' :
                start = MifkaCursor.atCurrentStart()
            else :
                start = MifkaCursor.parseCursor( startS )

            if endS == '' :
                end = MifkaCursor.atCurrentEnd()
            else :
                end = MifkaCursor.parseCursor( endS )

            ret = MifkaRange( start, end )

        else :
            startS = s.strip()
            if startS == '' :
                start = MifkaCursor.atCurrentStart()
            else :
                start = MifkaCursor.parseCursor( startS )

            ret = MifkaRange( start, start )

        return ret
