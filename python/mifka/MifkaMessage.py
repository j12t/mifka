#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import ubos.logging
import ubos.utils

class MifkaMessage :
    """
    The content of what's in the queue
    """

    def __init__(self, id, contentType, content ) :
        """
        Create.

        id: identifier, if known
        contentType: type of content
        content: content of the message
        """
        self.id          = id
        self.contentType = contentType
        self.content     = content


    def getId(self) :
        return self.id


    def getTimestamp(self) :
        return ubos.utils.time2string( float( self.id[1:] ) / 1000000 )


    def getContentType(self) :
        return self.contentType


    def getContent(self) :
        return self.content
