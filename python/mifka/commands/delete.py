#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaRange import MifkaRange
from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    service = MifkaService( db )

    topic = service.topicGet( args.topic )
    if topic is None:
        ubos.logging.fatal( 'Unknown topic:', args.topic )

    nDeleted = service.deleteRange( topic, MifkaRange.parseRange( args.range ))

    print( "Deleted %d messages" % nDeleted )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,              help='Delete some or all messages on this topic (did you mean purge?).' )
    parser.add_argument( 'topic',                           help='The topic')
    parser.add_argument( 'range', nargs=argparse.REMAINDER, help='The range to list' )
