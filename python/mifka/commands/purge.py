#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaCursor import MifkaCursor
from mifka.MifkaRange import MifkaRange
from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    service = MifkaService( db )

    topic = service.topicGet( args.topic )
    if topic is None:
        ubos.logging.fatal( 'Unknown topic:', args.topic )

    toCur = MifkaCursor.parseCursor( args.cursor )
    nPurged = service.deleteRange( topic, MifkaRange( MifkaCursor.atCurrentStart(), toCur ))

    print( "Purged %d messages" % nPurged )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName, help='Purge the oldest messages on this topic.' )
    parser.add_argument( 'topic',              help='The topic')
    parser.add_argument( 'cursor',             help='Indicates the oldest message to keep' )
