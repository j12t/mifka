#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    service = MifkaService( db )

    if service.topicDelete( args.topic, args.if_exists ) :
        print( "Topic deleted" )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName, help='Delete a topic.' )
    parser.add_argument( 'topic', help='The topic to delete')
    parser.add_argument('--if-exists', action='store_const', const=True, help='Only delete if this topic exists')
