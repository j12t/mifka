#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    if args.content is None:
        if args.file is None:
            ubos.logging.fatal( 'Must specify either content or file' )
        else:
            content = ubos.utils.slurpFile( args.file )
            if content is None:
                ubos.logging.fatal()
    else:
        if args.file is not None:
            ubos.logging.fatal( 'Specify content or file, not both' )
        else:
            content = args.content

    service = MifkaService( db )

    topic = service.topicGet( args.topic )
    if topic is None:
        ubos.logging.fatal( 'Unknown topic:', args.topic )

    if not service.publish( topic, args.type, content ) :
        ubos.logging.fatal( 'Publishing failed' )


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,  help='Publish to a topic.' )
    parser.add_argument( 'topic',               help='The topic to publish to')
    parser.add_argument( 'content', nargs='?',  help='The content to publish (alternatively use --file)')
    parser.add_argument( '--type',              help='Type of content, e.g. mime:application/json')
    parser.add_argument( '--file',              help='The file containing the content to publish' )
