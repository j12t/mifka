#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaRange import MifkaRange
from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    service = MifkaService( db )

    topic = service.topicGet( args.topic )
    if topic is None:
        ubos.logging.fatal( 'Unknown topic:', args.topic )

    messages = topic.getRange( MifkaRange.parseRange( args.range ))

    for message in messages:
        print( "----------------" )
        print( "id:           %s" % message.getId() )
        print( "timestamp:    %s" % message.getTimestamp() )
        print( "content-type: %s" % message.getContentType() )
        if not args.nocontent :
            print( "content:      (next line)" )
            print( message.getContent() )
        print()


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName,                            help='Show the content of a message' )
    parser.add_argument( '--nocontent', action='store_const', const=True, help='Only show metadata, skip content' )
    parser.add_argument( 'topic',                                         help='The topic')
    parser.add_argument( 'range', nargs=argparse.REMAINDER,               help='The range to list' )

