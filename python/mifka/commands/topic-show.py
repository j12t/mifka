#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import ubos.logging
import ubos.utils

from mifka.MifkaRange import MifkaRange
from mifka.MifkaService import MifkaService

def run(args, db, settings) :
    """
    Run this command.

    args: parsed command-line arguments
    db: database
    settings: settings for this mifka instance
    """

    service = MifkaService( db )
    topic = service.topicGet( args.topic )
    if topic is None:
        ubos.logging.fatal( 'Unknown topic:', args.topic )

    print( "Name:   " + topic.getName())
    print( "Length: " + str( len( topic.getRange( MifkaRange.fullRange() ))))


def addSubParser( parentParser, cmdName ) :
    """
    Enable this command to add its own command-line options
    parentParser: the parent argparse parser
    cmdName: name of this command
    """
    parser = parentParser.add_parser( cmdName, help='Show information about a topic.' )
    parser.add_argument( 'topic',              help='The topic to provide info on')
