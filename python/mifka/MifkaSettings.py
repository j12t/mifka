#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import ubos.logging

class MifkaSettings :

    def create(settingsFileName):
        """
        Factory method to find settings file and create settings object

        settingsFileName: name of the settings file to use
        """

        settingsJson = ubos.utils.readJsonFromFile( settingsFileName )
        if settingsJson is None:
            ubos.logging.fatal( 'Cannot read settings file', settingsFileName )

        return MifkaSettings( settingsJson )


    def __init__(self, settingsJson ) :
        self.settingsJson = settingsJson


    def getDbUser(self) :
        return self.settingsJson['db']['user']


    def getDbPass(self) :
        return self.settingsJson['db']['pass']


    def getDbHost(self) :
        return self.settingsJson['db']['host']


    def getDbName(self) :
        return self.settingsJson['db']['name']
