#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import argparse
import importlib
import mifka.commands
import os.path
import ubos.logging
import ubos.utils
import sys
from mifka.MifkaDb import MifkaDb
from mifka.MifkaSettings import MifkaSettings

def run():
    """
    Main entry point: looks for available subcommands and
    executes the correct one.
    """
    cmdNames = ubos.utils.findSubmodules(mifka.commands)

    parser = argparse.ArgumentParser( description='Simple/istic Kafka-like queue processor. Use <cursor> .. <cursor> (with spaces) to specify range.')
    parser.add_argument('-v', '--verbose', action='count',       default=0,  help='Display extra output. May be repeated for even more output.')
    parser.add_argument('--logConfig',                                       help='Use an alternate log configuration file for this command.')
    parser.add_argument('--debug',         action='store_const', const=True, help='Suspend execution at certain points for debugging' )
    parser.add_argument('--config',                                          help='Settings file to use' )
    cmdParsers = parser.add_subparsers( dest='command', required=True )

    cmds = {}
    for cmdName in cmdNames:
        mod = importlib.import_module('mifka.commands.' + cmdName)
        mod.addSubParser( cmdParsers, cmdName )
        cmds[cmdName] = mod

    args,remaining = parser.parse_known_args(sys.argv[1:])
    cmdName = args.command

    ubos.logging.initialize('mifka', cmdName, args.verbose, args.logConfig, args.debug)

    if len(remaining)>0 :
        parser.print_help()
        exit(0)

    settings = MifkaSettings.create( args.config )
    db       = MifkaDb( settings )

    if cmdName in cmdNames:
        try :
            ret = cmds[cmdName].run(args, db, settings)
            exit( ret )

        except Exception as e:
            ubos.logging.fatal( str(type(e)), '--', e )

        finally :
            db.close()

    else:
        ubos.logging.fatal('Sub-command not found:', cmdName, '. Add --help for help.' )


def run_not_implemented(args,settings):
    ubos.logging.fatal('Not implemented yet! Sorry. Want to help? https://gitlab.com/j12t/mifka')
