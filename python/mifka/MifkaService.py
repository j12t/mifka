#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import os.path
import ubos.logging
import ubos.utils
import sys
import time

from mifka.MifkaCursor import *
from mifka.MifkaMessage import MifkaMessage
from mifka.MifkaTopic import MifkaTopic

class MifkaService :
    def __init__(self, db) :
        """
        Instantiate a MifkaService

        db: use this Database
        """
        self.db = db


    def topicCreate(self, topic, ignoreIfExists=False ) :
        """
        Create a new topic

        topic: name of the new topic
        ignoreIfExists: silently do nothing if the topic exists already
        return: True if the topic was created, or existed already
        """
        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        try:
            cur.execute(
                    "INSERT INTO topic( name ) VALUES( %s )",
                    ( topic, )) # note extra comma, dear python

            dbComm.commit()

        except:
            if not ignoreIfExists :
                ubos.logging.error( 'Topic exists already:', topic )

        cur.close()

        return self.topicGet( topic )


    def topicDelete(self, topic, ignoreIfNotExists=False ) :
        """
        Delete a topic

        topic: name of the to-be-deleted topic
        ignoreIfNotExists: silently do nothing if the topic does not exist
        return: True if the topic was deleted
        """
        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        ret = False

        cur.execute(
                "DELETE FROM topic WHERE name = %s;",
                ( topic, )) # note extra comma, dear python
        ret = cur.rowcount > 0

        cur.execute(
                "DELETE FROM queue WHERE topic = %s;",
                ( topic, )) # note extra comma, dear python

        dbComm.commit()

        cur.close()

        if not ret and not ignoreIfNotExists :
            ubos.logging.error( 'No such topic:', topic )

        return ret


    def topicList(self) :
        """
        Obtain all topics

        return: list of Topic
        """
        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        cur.execute( "SELECT * FROM topic;" )

        ret = []
        for( id, name ) in cur :
            ret.append( MifkaTopic( id, name, self ))

        cur.close()

        return ret


    def topicGet(self, topic) :
        """
        Find a named topic

        topic: name of the topic
        return: Topic
        """
        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        cur.execute(
                "SELECT * FROM topic WHERE name = %s;",
                ( topic, )) # note extra comma, dear python

        ret = None
        for( id, name ) in cur :
            ret = MifkaTopic( id, name, self )
            break

        cur.close()

        return ret


    def publish( self, topic, contentType, content ) :
        """
        Publish content of type type to topic.

        topic: the topic
        contentType: the type of content
        content: the content
        return: True if successful
        """

        nowCursor = KeyMifkaCursor.fromTs( self._now() )

        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        ret = 0
        try:
            cur.execute(
                    "INSERT INTO queue( topic, id, type, content ) VALUES( %s, %s, %s, %s );",
                    ( topic.name, nowCursor.getId(), contentType, content ))

            dbComm.commit()

            ret = 1

        except Exception as e:
            ubos.logging.error( e )

        cur.close()

        return ret


    def republish( self, topic, message ) :
        """
        Republish a message to a new topic. This will cause the identifier
        to change, but everything else remaining the same

        msg: the message to republish
        return: True if successful
        """

        return self.publish( topic, message.getContentType(), message.getContent() )


    def getRange( self, topic, r ) :
        """
        Obtain the elements in this range for this topic

        topic: the topic
        r: the range
        return: array of MifkaMessage
        """

        ubos.logging.trace( 'MifkaService.getRange', r )

        # OffsetMifkaCursors are a bit hard to do in SQL ...
        # Mixing with KeyMifkaCursors is also messy ...
        # So we do all of them on the client

        sql  = "SELECT id, type, content FROM queue WHERE topic = %s ORDER BY id;"
        args = ( topic.getName(), )

        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        cur.execute( sql, args )

        rows = cur.fetchall() # need to get the number of rows

        cur.close()

        n = len( rows )

        startOffset = 0
        endOffset   = n
        startId     = None
        endId       = None

        def smallerEqOp( a, b ) :
            return a <= b

        def smallerOp( a, b ) :
            return a < b

        def largerEqOp( a, b ) :
            return a >= b

        def largerOp( a, b ) :
            return a > b

        if r.getStart().getBeforeAfterHere() == BeforeAfterHere.AFTER :
            startCompOp = largerOp

        else: # must be HERE
            startCompOp = largerEqOp

        if r.getEnd().getBeforeAfterHere() == BeforeAfterHere.BEFORE :
            endCompOp = smallerOp

        else: # must be HERE
            endCompOp = smallerEqOp


        if isinstance( r.getStart(), OffsetFromStartMifkaCursor ) :
            startOffset = r.getStart().getOffset()

        elif isinstance( r.getStart(), OffsetFromEndMifkaCursor ) :
            startOffset = n - r.getStart().getOffset()

        elif isinstance( r.getStart(), KeyMifkaCursor ) :
            startId = r.getStart().getId()

        else:
            ubos.logging.fatal( 'Unkonwn type of cursor' )

        if isinstance( r.getEnd(), OffsetFromStartMifkaCursor ) :
            endOffset = r.getEnd().getOffset()

        elif isinstance( r.getEnd(), OffsetFromEndMifkaCursor ) :
            endOffset = n - r.getEnd().getOffset()

        elif isinstance( r.getEnd(), KeyMifkaCursor ) :
            endId = r.getEnd().getId()

        else:
            ubos.logging.fatal( 'Unkonwn type of cursor' )

        ret = []
        i = 0
        for( id, type, content ) in rows :
            if ( startCompOp( i, startOffset )
            and  endCompOp(   i, endOffset )
            and  ( startId is None or startCompOp( id, startId ))
            and  ( endId   is None or endCompOp(   id, endId ))) :

                ret.append( MifkaMessage( id, type, content ))

            i += 1

        return ret


    def deleteRange( self, topic, r ) :
        """
        Delete the elements in this range for this topic

        topic: the topic
        r: the range
        return: number of deleted messages
        """

        # bit of a cop-out, but can be implemented quickly :-)
        toDelete = self.getRange( topic, r )

        if len( toDelete ) == 0:
            return 0

        dbComm = self.db.connect()
        cur    = dbComm.cursor()

        cur.execute(
                'DELETE FROM queue WHERE topic = %s AND id >= %s AND id <= %s',
                ( topic.getName(), toDelete[0].getId(), toDelete[-1].getId() ))

        dbComm.commit()

        ret = cur.rowcount

        cur.close()

        return ret


    def _now(self) :
        """
        Helper for high-res current time

        return: integer
        """
        return int( time.time() * 1000000 )
