#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

import mysql.connector

class MifkaDb :
    """
    Encapsulates Database access
    """
    def __init__( self, settings ) :
        """
        Create.

        settings: get database info from here
        """
        self.settings     = settings
        self.dbConnection = None

    def connect( self ) :
        """
        Smart factory to obtain a MySQL "Cursor" object

        """
        if self.dbConnection is None:
            self.dbConnection = mysql.connector.connect(
                    user     = self.settings.getDbUser(),
                    password = self.settings.getDbPass(),
                    host     = self.settings.getDbHost(),
                    database = self.settings.getDbName() )

        return self.dbConnection

    def close( self ) :
        if self.dbConnection is None:

            self.dbConnection.close()
            self.dbConnection = None

