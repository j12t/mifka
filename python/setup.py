#!/usr/bin/python
#
# Copyright (C) 2020 and later, Johannes Ernst. All rights reserved. License: see package.
#

from setuptools import setup
import mifka

setup(name='mifka',
      author='Johannes Ernst',
      license='AGPLv3',
      description='Minimal queueing app',
      url='http://gitlab.com/j12t/mifka',
      packages=[
          'mifka',
          'mifka.commands'
      ],
      zip_safe=True)
